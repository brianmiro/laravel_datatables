   @extends('welcome')
   @section('content')
   
    <div class="row">
             <!--   right column -->
            <div class="col-xs-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Datatables</h3>&nbsp;    
                              
                        </div>
                         <!--  /.box-header -->
                              <div class="box-body">
                                            <table id="tableData"  class="table  table-condensed table-bordered table-striped table-hover">
                                                    <thead style="background-color:#BDC4C4;">
                                                            <tr>
                                                                    <th>Post Id</th>
                                                                    <th>Id</th>  
                                                                    <th>Name</th>   
                                                                    <th>Email</th>      
                                                                                                                                                                               
                                                            </tr>
                                                    </thead>                                                    
                                            </table>  
                                 </div> 
                               <!--   /.box-body -->     
                     </div>  
                   <!-- /.box-->
           </div>  

 </div>  
    <style>
        .table-hover>tbody>tr:hover>td, .table-hover>tbody>tr:hover>th {
        background-color:#D8D8D8;}
        .custom-loader-color  
        {  
        color: #F43420 !important;  
        font-size: 19px !important;  
        }  
        .custom-loader-background  
        {  
        background-color: #FFF !important;  
        font-size: 19px !important;  
        }     
    </style>
    @endsection
@section('javascript')      
<script>
    
            $(document).ready(function() {

               var table= $('#tableData').DataTable({
                    processing: true,    
                    autoWidth: false,
                    order: [[ 0, "desc" ]],    
                    scrollX: true,       
                    dom: 'Bfltpri',//Esta opcion permite que se vean los botones de exportacion y la barra de filtros                  
                    buttons: [ 'csv', 'excel', 'print','pdf','colvis'],
                    ajax: {
                       url : "https://jsonplaceholder.typicode.com/comments", //ruta de donde obtiene los datos para llenar la tabla
                       dataSrc: '',
                       global:true,
                    },            
                    columns: [
                        {"data": "postId"},            //valores que van en cada columba        
                        {"data": "id"},
                        {"data": "name"},
                        {"data": "email"},           

                    ],
                    "language": {
                        "info": "_TOTAL_ registros",
                        "search": "Buscar",
                        "processing": "<div class='custom-loader-background'><i class='fa fa-cog fa-spin custom-loader-color'></i>&nbsp;Procesando...</div>",
                        "paginate": {
                            "next": "Siguiente",
                            "previous": "Anterior",
                        },
                        "buttons": {
                                    colvis: '<i class="fa fa-chevron-down"></i>',
                                    print:'Imprimir'
                        },
                        "lengthMenu": 'Mostrar <select style="background-color:#ffff;border: 1px solid #5F6A6A;">'+
                                     '<option value="10">10</option>'+
                                     '<option value="30">30</option>'+
                                     '<option value="50">50</option>'+
                                     '<option value="-1">Todos</option>'+
                                    '</select> registros',
                        "loadingRecords": "Cargando...",               
                        "emptyTable": "No hay datos",
                        "zeroRecords": "No hay coincidencias", 
                        "infoEmpty": "",
                        "infoFiltered": ""
                    }
                });
                table.buttons().container().appendTo( '#example_wrapper .col-sm-6:eq(0)' );
            });
        </script>  
    @endsection