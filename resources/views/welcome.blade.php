<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel Datatables.net</title>

          <!-- Bootstrap CSS -->
        <link href="{{ asset('adminlte/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
          <!-- Font Awesome -->
        <link href="{{ asset('adminlte/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">  
        
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
    <!-- Main content -->
    <section class="content container-fluid">
                @yield('content')
                </section>
            <!-- jQuery 3 -->
            <script src="{{ asset('datatables/jquery/dist/jquery.min.js') }}" > </script>
            <!--DataTables js-->
            <script src="{{ asset('datatables/datatables.net/js/jquery.dataTables.min.js') }}" ></script>
            <!--DataTables bootstrap js-->
            <script src="{{ asset('datatables/datatables.net-bs/js/dataTables.bootstrap.min.js') }}" ></script>
            <!--DataTables Buttons Export(Excel-PDF-CSV-Print) js-->
            <script src="{{ asset('datatables/datatables.plugins/dataTables.buttons.min.js')}}"></script>
            <script src="{{ asset('datatables/datatables.plugins/buttons.html5.min.js')}}"></script>
            <script src="{{ asset('datatables/datatables.plugins/buttons.print.min.js')}}"></script>
            <script src="{{ asset('datatables/datatables.plugins/jszip.min.js')}}"></script>
            <script src="{{ asset('datatables/datatables.plugins/pdfmake.min.js')}}"></script>
            <script src="{{ asset('datatables/datatables.plugins/vfs_fonts.js')}}"></script>
            <script src="{{ asset('datatables/datatables.plugins/buttons.bootstrap.min.js')}}"></script>
            <script src="{{ asset('datatables/datatables.plugins/buttons.colVis.min.js')}}"></script>

            @yield('javascript')
    </body>
</html>
